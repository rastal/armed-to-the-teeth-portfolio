﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class RotatingEnemyShip : MonoBehaviour {
    
    [SerializeField] int _scoreValue = 1;
    
    [Space]
    [SerializeField] float _maxHealth = 4f;
    
    GameLogic _gameLogic;
    AudioManager _audio;
    RotatingEnemyGroup _rotatingEnemyGroup;
    float _currentHealth;

    void OnEnable() {
        _gameLogic = GameObject.Find("Game Manager").GetComponent<GameLogic>();
        _audio = GameObject.Find("Audio Manager").GetComponent<AudioManager>();
        _rotatingEnemyGroup = this.GetComponentInParent<RotatingEnemyGroup>();
    }

    void Start() {
        _currentHealth = _maxHealth * _gameLogic.EnemyHealthMultiplier;
    }

    void OnCollisionEnter2D(Collision2D collision) {
        if (collision.gameObject.tag == "Player") {
            collision.gameObject.GetComponent<PlayerController>().Damage();
        }
    }

    void Update() {
        this.transform.rotation = Quaternion.identity;
        
        if (this.transform.position.x < -10f) {
            Destroy(this.gameObject);
        }
    }

    public void Damage(float damage) {
        _currentHealth -= damage;
        if (_currentHealth <= 0f) {
            _audio.DeathExplosion();
            _gameLogic.Score += _scoreValue;
            Destroy(this.gameObject);
        } else {
            _audio.EnemyHit();
        }
    }
}
