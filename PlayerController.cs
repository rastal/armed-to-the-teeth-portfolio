﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using DG.Tweening;
using TMPro;

public class PlayerController : MonoBehaviour {
    
    public int RemainingLives = 2;

    [SerializeField] float _shipSpeed = 5f;
    [SerializeField] int _startingLives = 2;
    [Tooltip("How long the player remains invincible after dying")]
    [SerializeField] float _invincibilityDuration = 2f;

    // TODO(Rastal): make the weapons separate from the PlayerController script?
    [Space]

    [Header("Default Gun")]
    [SerializeField] float _defaultFireRate = 2f;  // How many times it shoots per second
    [SerializeField] float _defaultDamage = 2f;
    [Space]

    [Header("Machine Gun")]
    [SerializeField] float _machineGunFireRate = 4f;
    [SerializeField] float _machineGunDamage = 2f;
    [SerializeField] int _machineGunMaxAmmo = 15;
    [SerializeField] float _machineGunReloadDuration = 2f;
    [Space]
    [SerializeField] GameObject _bullet;
    [SerializeField] ParticleSystem _damageParticles;

    Rigidbody2D _rigidbody;
    SpriteRenderer _sprite;
    Controls _input;
    GameLogic _gameLogic;
    AudioManager _audio;
    CustomDebug _debug;
    TextMeshProUGUI _feedback;
    GameObject _pauseMenu;
    Vector3 _startingPosition;
    float _nextShotTime = 0f;
    float _machineGunReloadedTime = 0f;
    float _damageMultiplier = 1f;
    float _fireRateMultiplier = 1f;
    bool _canSwitch = true;
    bool _reloading = false;
    bool _invincible = false;
    float _invincibleTime = 0f;
    Vector2 _collisionDirection = new Vector2(0f, 0f);
    bool _isPausing = false;
    bool _isPaused = false;

    int _currentWeapon = 0;
    [SerializeField] List<string> _heldWeapons = new List<string>() { "Default" };
    List<string> _availableWeapons = new List<string>() { "Machine Gun", "Laser", "Rocket Launcher", "Homing Missiles" };
    int _remainingMachineGunAmmo;

    void Awake() {
        _input = new Controls();
    }

    void OnEnable() {
        _input.Game.Enable();
        
        _debug = GameObject.Find("Debug Text").GetComponent<CustomDebug>();
        _gameLogic = GameObject.Find("Game Manager").GetComponent<GameLogic>();
        _audio = GameObject.Find("Audio Manager").GetComponent<AudioManager>();
        _rigidbody = this.GetComponent<Rigidbody2D>();
        _feedback = this.GetComponentInChildren<TextMeshProUGUI>();
        _sprite = this.GetComponent<SpriteRenderer>();
        _pauseMenu = GameObject.Find("Pause Menu");
    }

    void OnDisable() {
        _input.Game.Disable();
    }

    void Start() {
        _remainingMachineGunAmmo = _machineGunMaxAmmo;
        _feedback.text = "";
        RemainingLives = _startingLives;
        _startingPosition = this.transform.position;
        _pauseMenu.SetActive(false);
    }

    public void Restart() {
        _remainingMachineGunAmmo = _machineGunMaxAmmo;
        _feedback.text = "";
        RemainingLives = _startingLives;
        _sprite.enabled = true;
        this.transform.position = _startingPosition;
        _input.Menu.Disable();
        _input.Game.Enable();
    }

    void OnCollisionEnter2D(Collision2D collision) {
        if (collision.gameObject.layer == 8) {      // Layer 8 is the "border" area that we don't want to move past
            switch (collision.gameObject.name) {
                case "Left":
                    _collisionDirection.x = -1f;
                    break;
                case "Right":
                    _collisionDirection.x = 1f;
                    break;
                case "Top":
                    _collisionDirection.y = 1f;
                    break;
                case "Bottom":
                    _collisionDirection.y = -1f;
                    break;
            }
        }
    }

    void OnCollisionExit2D(Collision2D collision) {
        if (collision.gameObject.layer == 8) {
            switch (collision.gameObject.name) {
                case "Left":
                case "Right":
                    _collisionDirection.x = 0f;
                    break;
                case "Top":
                case "Bottom":
                    _collisionDirection.y = 0f;
                    break;
            }
        }
    }
    
    void Update() {

        if (Time.time > _invincibleTime) {
            _invincible = false;
        }
        if (Time.time > _machineGunReloadedTime && _reloading) {
            _remainingMachineGunAmmo = _machineGunMaxAmmo;
            _feedback.text = "";
            _reloading = false;
        }

        // Upgrade input
        if (_gameLogic.Upgrading) {
            _sprite.enabled = false;
            _input.Game.Disable();
            _input.Menu.Enable();
            _input.Menu.Submit.Disable();
            if (!_gameLogic.MenuInputAllowed && _input.Menu.Submit.ReadValue<float>() == 0f) {
                _gameLogic.MenuInputAllowed = true;
            }
        }
        // GameOver input
        if (_gameLogic.IsGameOver) {
            _input.Menu.Submit.Disable();
            if (!_gameLogic.MenuInputAllowed && _input.Menu.Submit.ReadValue<float>() == 0f) {
                _gameLogic.MenuInputAllowed = true;
            }
        }

        // Pause
        if (_input.Game.Pause.ReadValue<float>() == 1f && !_isPausing) {
            _isPausing = true;
            if (!_isPaused) {
                Pause();
            } else {
                Unpause();
            }
        } else if (_input.Game.Pause.ReadValue<float>() == 0f) {
            _isPausing = false;
        }

        // Bang bang
        if (_input.Game.Fire.ReadValue<float>() == 1f && Time.time > _nextShotTime && !_reloading) {
            switch (_heldWeapons[_currentWeapon]) {
                case "Default":
                    _audio.ShootDefault();
                    _nextShotTime = Time.time + 1f / (_defaultFireRate * _fireRateMultiplier);
                    var defaultBullet = Instantiate(_bullet, this.transform.position, Quaternion.identity);
                    defaultBullet.GetComponent<Bullet>().Damage = _defaultDamage * _damageMultiplier;
                    break;
                case "Machine Gun":
                    if (_remainingMachineGunAmmo > 0) {
                        _audio.ShootDefault();
                        _nextShotTime = Time.time + 1f / (_machineGunFireRate * _fireRateMultiplier);
                        var machineGunBullet = Instantiate(_bullet, this.transform.position, Quaternion.identity);
                        machineGunBullet.GetComponent<Bullet>().Damage = _machineGunDamage * _damageMultiplier;
                        --_remainingMachineGunAmmo;
                    } else if (_remainingMachineGunAmmo <= 0) {
                        _machineGunReloadedTime = Time.time + _machineGunReloadDuration;
                        _feedback.text = "reloading";
                        _reloading = true;
                    }
                    break;
                default:
                    break;
            }
        }

        // Reloading
        if (_input.Game.Reload.ReadValue<float>() == 1f && _heldWeapons[_currentWeapon] == "Machine Gun") {
            if (_heldWeapons[_currentWeapon] == "Machine Gun") {
                _machineGunReloadedTime = Time.time + _machineGunReloadDuration;
            }
            _feedback.text = "reloading";
            _reloading = true;
        }

        // Weapon Switching
        var weaponSwitch = (int)_input.Game.SwitchWeapons.ReadValue<float>();
        if (weaponSwitch == 0) {
            _canSwitch = true;
        } else if (_canSwitch) {
            _canSwitch = false;
            _currentWeapon += weaponSwitch;
            _currentWeapon = Mathf.Abs(_currentWeapon % _heldWeapons.Count);
        }

        // Movement
        var moveInput = _input.Game.Move.ReadValue<Vector2>();
        if ((moveInput.x < 0f && _collisionDirection.x < 0f) ||
            (moveInput.x > 0f && _collisionDirection.x > 0f)) {
            moveInput.x = 0f;
        }
        if ((moveInput.y < 0f && _collisionDirection.y < 0f) ||
            (moveInput.y > 0f && _collisionDirection.y > 0f)) {
            moveInput.y = 0f;
        }
        var move = new Vector3(moveInput.x, moveInput.y, 0f) * Time.deltaTime * _shipSpeed;
        this.transform.Translate(move, Space.World);

        // Custom Debug Display
        for (int i = 0; i < _heldWeapons.Count; ++i){
            _debug.Display("Held Weapon " + i, _heldWeapons[i].ToString());
        }
        _debug.Display("Current Machine Gun Ammo", _remainingMachineGunAmmo.ToString());
    }

    public void Damage() {
        if (!_invincible) {
            _damageParticles.Play();
            if (RemainingLives > 1) {
                --RemainingLives;
                _invincible = true;
                _invincibleTime = Time.time + _invincibilityDuration;
                _sprite.DOFade(0f, _invincibilityDuration).SetEase(Ease.Flash, 20f).Play();
                _audio.PlayerHit();
            } else {
                _audio.DeathExplosion();
                _sprite.enabled = false;
                _gameLogic.MenuInputAllowed = false;
                _input.Game.Disable();
                _input.Menu.Enable();
                _gameLogic.GameOver();
            }
        }
    }

    public void UpgradeShip(string name) {
        switch (name) {
            case "life":
                ++RemainingLives;
                break;
            case "speed":
                _shipSpeed *= 1f + _gameLogic.SpeedIncrease;
                break;
            case "fire rate":
                _fireRateMultiplier *= 1f + _gameLogic.FireRateIncrease;
                break;
            case "shield":
                // absorb one hit while reloading
                break;
            case "damage":
                _damageMultiplier *= 1f + _gameLogic.DamageIncrease;
                break;
            case "energy stealer":
                // after hitting many enemies, one life is refilled;
                // make this a weapon?
                break;
            case "score":
                _gameLogic.Score += _gameLogic.ScoreIncrease;
                break;
            default:
                break;
        }

        _audio.Upgrade();
        _gameLogic.Upgrading = false;
        _sprite.enabled = true;
        _input.Menu.Disable();
        _input.Game.Enable();
    }

    void Pause() {
        _isPaused = true;
        Time.timeScale = 0f;
        _pauseMenu.SetActive(true);
    }

    void Unpause() {
        _pauseMenu.SetActive(false);
        Time.timeScale = 1f;
        _isPaused = false;
    }
}
