﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class RotatingEnemyGroup : MonoBehaviour {
    
    [SerializeField] GameObject _shipPrefab;
    [SerializeField] float _movementSpeed = 2f;
    [SerializeField] float _circleRadius = 1f;
    [Tooltip("How long it takes to complete a rotation")]
    [SerializeField] float _baseRotationLength = 5f;
    
    [Space]
    [SerializeField] int _minimumShips = 2;
    [SerializeField] int _maximumShips = 8;
    GameLogic _gameLogic;
    float _rotateDuration;
    int _shipNumber;

    void OnEnable() {
        _shipNumber = Random.Range(_minimumShips, _maximumShips + 1);
        _rotateDuration = _baseRotationLength / _shipNumber;
        _gameLogic = GameObject.Find("Game Manager").GetComponent<GameLogic>();
    }

    void Start() {
        var angleIncrement = 360f / _shipNumber;
        for (int i = 1; i <= _shipNumber; ++i) {
            var angle = angleIncrement * i * Mathf.Deg2Rad;
            var x = this.transform.position.x + _circleRadius * Mathf.Cos(angle);
            var y = this.transform.position.y + _circleRadius * Mathf.Sin(angle);
            var position = new Vector2(x, y);
            Instantiate(_shipPrefab, position, Quaternion.identity, this.transform);
        }

        Sequence rotation = DOTween.Sequence();
        rotation.Append(this.transform.DORotate(new Vector3(0f, 0f, -180f), _rotateDuration).SetEase(Ease.Linear));
        rotation.Append(this.transform.DORotate(new Vector3(0f, 0f, -360f), _rotateDuration).SetEase(Ease.Linear));
        rotation.SetLoops(-1).SetLink(this.gameObject).Play();
    }

    void Update() {
        this.transform.Translate(new Vector3(-1f, 0f, 0f) * _movementSpeed * Time.deltaTime, Space.World);

        if (this.transform.position.x < -10f) {
            Destroy(this.gameObject);
            --_gameLogic.EnemiesLeftBeforeUpgrade;
        }
    }
}
