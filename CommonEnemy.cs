﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class CommonEnemy : MonoBehaviour {
    
    [SerializeField] int _scoreValue;
    [Space]
    [SerializeField] float _maxHealth = 4f;
    [SerializeField] float _speed = 2f;
    [SerializeField] float _fireRate = 1f;
    [SerializeField] GameObject _bulletPrefab;

    [Header("Y Movement")]

    [Tooltip("Keep this at 0 for enemies that don't move back and forth")]
    [Range(0f, 10f)]
    [SerializeField] float _yMovementRange = 0f;
    [SerializeField] float _yMovementDuration = 1f;
    [Tooltip("Use one of the InOut functions for best results")]
    [SerializeField] Ease _yMovementShape = Ease.InOutQuad;
    
    GameLogic _gameLogic;
    AudioManager _audio;
    float _nextFire = 0f;
    float _currentHealth;
    float _yPosition;

    void OnEnable() {
        _gameLogic = GameObject.Find("Game Manager").GetComponent<GameLogic>();
        _audio = GameObject.Find("Audio Manager").GetComponent<AudioManager>();
        _yPosition = this.transform.position.y;
    }

    void Start() {
        _currentHealth = _maxHealth * _gameLogic.EnemyHealthMultiplier;
        if (_yMovementRange > 0) {
            this.transform.DOMoveY(_yMovementRange, _yMovementDuration).SetEase(_yMovementShape)
                .SetLoops(-1, LoopType.Yoyo).SetLink(this.gameObject).Play();
        }
    }

    void OnCollisionEnter2D(Collision2D collision) {
        if (collision.gameObject.tag == "Player") {
            collision.gameObject.GetComponent<PlayerController>().Damage();
        }
    }

    void Update() {
        this.transform.Translate(new Vector3(-1f, 0f, 0f) * _speed * Time.deltaTime);
        
        if (Time.time > _nextFire) {
            var bullet = Instantiate(_bulletPrefab, this.transform.position, Quaternion.identity);
            _audio.ShootDefault();
            _nextFire = Time.time + 1f / _fireRate;
        }

        if (this.transform.position.x < -10f) {
            Destroy(this.gameObject);
            --_gameLogic.EnemiesLeftBeforeUpgrade;
        }
    }

    public void Damage(float damage) {
        _currentHealth -= damage;
        if (_currentHealth <= 0f) {
            _audio.DeathExplosion();
            _gameLogic.Score += _scoreValue;
            --_gameLogic.EnemiesLeftBeforeUpgrade;
            Destroy(this.gameObject);
        } else {
            _audio.EnemyHit();
        }
    }
}
