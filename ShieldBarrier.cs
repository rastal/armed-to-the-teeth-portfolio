﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShieldBarrier : MonoBehaviour {

    [SerializeField] float _speed = 1f;

    [Space]
    [SerializeField] float _minimumHeight = 0.5f;
    [SerializeField] float _maximumHeight = 5f;
    [SerializeField] float _width = 0.5f;
    GameLogic _gameLogic;

    void OnEnable() {
        _gameLogic = GameObject.Find("Game Manager").GetComponent<GameLogic>();
    }

    void Start() {
        var height = Random.Range(_minimumHeight, _maximumHeight);
        this.transform.localScale = new Vector3(_width, height, 1f);
    }

    void OnCollisionEnter2D(Collision2D collision) {
        if (collision.gameObject.tag == "Player") {
            collision.gameObject.GetComponent<PlayerController>().Damage();
        }
    }

    void Update() {
        this.transform.Translate(new Vector3(-1f, 0f, 0f) * _speed * Time.deltaTime);
        
        if (this.transform.position.x < -10f) {
            Destroy(this.gameObject);
            --_gameLogic.EnemiesLeftBeforeUpgrade;
        }
    }
}
