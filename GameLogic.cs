﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using TMPro;

public class GameLogic : MonoBehaviour {
    
    [HideInInspector] public int Score = 0;
    [HideInInspector] public bool IsGameOver = false;
    [HideInInspector] public float EnemyHealthMultiplier = 1f;
    [HideInInspector] public bool Upgrading = false;
    [HideInInspector] public bool MenuInputAllowed = false;
    [HideInInspector] public int EnemiesLeftBeforeUpgrade = 0;
    
    const float _minSpawnY = -5f;
    const float _maxSpawnY = 5f;
    const float _spawnX = 10f;

    [SerializeField] int _startingDifficulty = 1;
    [Tooltip("How much enemy health increases when difficulty increases")]
    [SerializeField] float _enemyHealthIncrease = 0.5f;

    [Space]
    [SerializeField] int _upgradesBetweenDifficultyIncrease = 4;
    [SerializeField] int _enemiesBetweenUpgrades = 10;
    [SerializeField] float _startingSpawnRate = 5f;
    [SerializeField] float _maximumSpawnRate = 30f;
    [SerializeField] float _spawnRateIncrease = 1f;

    [Space] 
    [Tooltip("Make sure shield barriers are last on this list")]
    [SerializeField] List<GameObject> _spawnPrefabList = new List<GameObject>();
    
    [Space]
    [SerializeField] List<string> _upgradeDescriptionList = new List<string>();
    public float SpeedIncrease;
    public float FireRateIncrease;
    public float DamageIncrease;
    public int ScoreIncrease;

    int _upgradeCount = 0;
    int _currentDifficulty;
    float _currentSpawnRate;
    float _spawnInterval = 20f;
    float _nextSpawnTime;
    int _enemiesLeftToSpawn = 0;
    string _upgrade1Name = "";
    string _upgrade2Name = "";

    PlayerController _player;
    Controls _input;
    
    TextMeshProUGUI _remainingLivesDisplay;
    TextMeshProUGUI _enemiesLeftDisplay;
    TextMeshProUGUI _scoreDisplay;

    GameObject _hud;
    GameObject _gameOverMenu;
    TextMeshProUGUI _highScoreDisplay;
    TextMeshProUGUI _yourScoreDisplay;

    GameObject _upgradeMenu;
    TextMeshProUGUI _upgradeText1;
    TextMeshProUGUI _upgradeText2;

    void Awake() {
        _input = new Controls();
    }

    void OnEnable() {
        _remainingLivesDisplay = GameObject.Find("Remaining Lives").GetComponent<TextMeshProUGUI>();
        _enemiesLeftDisplay = GameObject.Find("Enemies Left").GetComponent<TextMeshProUGUI>();
        _scoreDisplay = GameObject.Find("Score").GetComponent<TextMeshProUGUI>();

        _player = GameObject.FindWithTag("Player").GetComponent<PlayerController>();
        _upgradeMenu = GameObject.Find("Upgrade Menu");
        _hud = GameObject.Find("HUD");
        _gameOverMenu = GameObject.Find("Game Over");
    }

    void Start() {
        _currentDifficulty = _startingDifficulty;
        _currentSpawnRate = _startingSpawnRate;
        for (int i = 1; i < _startingDifficulty; ++i) {
            EnemyHealthMultiplier *= _enemyHealthIncrease + 1;
            _currentSpawnRate += _spawnRateIncrease;
        }
        
        _upgradeMenu.SetActive(false);
        _gameOverMenu.SetActive(false);
        _hud.SetActive(true);

        _enemiesLeftToSpawn = _enemiesBetweenUpgrades;
        EnemiesLeftBeforeUpgrade = _enemiesBetweenUpgrades;
        Score = 0;
    }

    public void Restart() {
        _currentDifficulty = _startingDifficulty;
        _currentSpawnRate = _startingSpawnRate;
        for (int i = 1; i < _startingDifficulty; ++i) {
            EnemyHealthMultiplier *= _enemyHealthIncrease + 1;
            _currentSpawnRate += _spawnRateIncrease;
        }
        
        _upgradeMenu.SetActive(false);
        _gameOverMenu.SetActive(false);
        _hud.SetActive(true);
        _input.Menu.Disable();
        _input.Game.Enable();

        _enemiesLeftToSpawn = _enemiesBetweenUpgrades;
        EnemiesLeftBeforeUpgrade = _enemiesBetweenUpgrades;
        Score = 0;
        _upgradeCount = 0;
        _player.Restart();
        IsGameOver = false;
    }

    void Update() {
        if (!IsGameOver && !Upgrading) {
            if (Time.time > _nextSpawnTime && _enemiesLeftToSpawn > 0) {
                var y = Random.Range(_minSpawnY, _maxSpawnY);
                int i;
                if (_currentDifficulty < 4) {
                    i = Random.Range(0, _spawnPrefabList.Count - 1);
                } else {
                    i = Random.Range(0, _spawnPrefabList.Count);
                }
                Instantiate(_spawnPrefabList[i], new Vector3(_spawnX, y, 0f), Quaternion.identity);
                _nextSpawnTime = Time.time + (_spawnInterval / _currentSpawnRate);
                --_enemiesLeftToSpawn;
            }
            if (Time.time > _nextSpawnTime && _enemiesLeftToSpawn <= 0 && 
                GameObject.FindGameObjectsWithTag("Enemy").Length == 0) {
                
                UpgradeShip();
                if (_upgradeCount >= _upgradesBetweenDifficultyIncrease) {
                    IncreaseDifficulty();
                    Debug.Log("Difficulty Increased");
                }
                Debug.Log("Upgrades since last Difficulty Increase: " + _upgradeCount);
            }
        }
    }

    void LateUpdate() {
        if (_remainingLivesDisplay != null) {
            _remainingLivesDisplay.text = "Remaining Lives: " + _player.RemainingLives;
        }
        if (_enemiesLeftDisplay != null) {
            string enemiesLeftText;
            if (EnemiesLeftBeforeUpgrade > 0) {
                enemiesLeftText = EnemiesLeftBeforeUpgrade.ToString();
            } else {
                enemiesLeftText = "0";
            }
            _enemiesLeftDisplay.text = "Enemies Left: " + enemiesLeftText;
        }
        if (_scoreDisplay != null) {
            _scoreDisplay.text = Score.ToString();
        }
    }

    void UpgradeShip() {
        Upgrading = true;
        MenuInputAllowed = false;
        _enemiesLeftToSpawn = _enemiesBetweenUpgrades;
        EnemiesLeftBeforeUpgrade = _enemiesBetweenUpgrades;
        ++_upgradeCount;
        
        var bullets = GameObject.FindGameObjectsWithTag("Projectile");
        if (bullets.Length > 0) {
            foreach (GameObject bullet in bullets) {
                Destroy(bullet);
            }
        }

        _hud.SetActive(false);
        
        _upgradeMenu.SetActive(true);
        EventSystem.current.SetSelectedGameObject(GameObject.Find("Upgrade 1"));
        _upgradeText1 = GameObject.Find("Upgrade 1").GetComponentInChildren<TextMeshProUGUI>();
        _upgradeText2 = GameObject.Find("Upgrade 2").GetComponentInChildren<TextMeshProUGUI>();

        var i = Random.Range(0, _upgradeDescriptionList.Count);
        var upgrade1 = _upgradeDescriptionList[i];
        _upgradeText1.text = upgrade1;
        _upgrade1Name = NameUpgrade(upgrade1);

        var j = Random.Range(0, _upgradeDescriptionList.Count);
        if (j == i) {
            ++j;
            j %= _upgradeDescriptionList.Count;
        }
        var upgrade2 = _upgradeDescriptionList[j];
        _upgradeText2.text = upgrade2;
        _upgrade2Name = NameUpgrade(upgrade2);
    }

    void IncreaseDifficulty() {
        ++_currentDifficulty;
        EnemyHealthMultiplier *= _enemyHealthIncrease + 1f;
        if (_currentSpawnRate < _maximumSpawnRate) {
            _currentSpawnRate += _spawnRateIncrease;
        }
        _upgradeCount = 0;
        // Display message that the enemy health has increased and enemies will
        // spawn more frequently now
    }

    public void GameOver() {
        IsGameOver = true;
        var enemies = GameObject.FindGameObjectsWithTag("Enemy");
        if (enemies.Length > 0) {
            foreach (GameObject enemy in enemies) {
                Destroy(enemy);
            }
        }

        if (Score > PlayerPrefs.GetInt("High Score", 0)) {
            PlayerPrefs.SetInt("High Score", Score);
            PlayerPrefs.Save();
        }
        
        _hud.SetActive(false);
        _gameOverMenu.SetActive(true);
        EventSystem.current.SetSelectedGameObject(GameObject.Find("Retry Button"));
        _highScoreDisplay = GameObject.Find("High Score").GetComponent<TextMeshProUGUI>();
        _highScoreDisplay.text = PlayerPrefs.GetInt("High Score", 0).ToString();
        _yourScoreDisplay = GameObject.Find("Your Score").GetComponent<TextMeshProUGUI>();
        _yourScoreDisplay.text = Score.ToString();
    }

    string NameUpgrade(string upgrade) {
        var text = "";
        
        if (upgrade.Contains("life")) {
            text = "life";
        } else if (upgrade.Contains("speed")) {
            text = "speed";
        } else if (upgrade.Contains("rate")) {
            text = "fire rate";
        } else if (upgrade.Contains("damage")) {
            text = "damage";
        } else if (upgrade.ToLower().Contains("score")) {
            text = "score";
        }

        return text;
    }

    public void Upgrade1() {
        _player.UpgradeShip(_upgrade1Name);
        _upgradeMenu.SetActive(false);
        _hud.SetActive(true);
    }

    public void Upgrade2() {
        _player.UpgradeShip(_upgrade2Name);
        _upgradeMenu.SetActive(false);
        _hud.SetActive(true);
    }
}
