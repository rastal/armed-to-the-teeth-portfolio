﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour {
    
    [SerializeField] AudioSource _playerShootingDefault;
    [SerializeField] AudioSource _playerHit;
    [SerializeField] AudioSource _enemyHit;
    [SerializeField] AudioSource _deathExplosion;
    [SerializeField] AudioSource _upgrade;
    [SerializeField] AudioSource _powerUp;
    
    [Header("Other player weapons")]
    [SerializeField] AudioSource _machineGun;
    [SerializeField] AudioSource _rocketLaunched;
    [SerializeField] AudioSource _missileLaunched;
    [SerializeField] AudioSource _laserBeam;

    void PlaySound(AudioSource audio) {
        var soundVariation = Random.Range(0f, 0.1f);
        var originalPitch = audio.pitch;
        var originalVolume = audio.volume;
        
        audio.pitch -= soundVariation;
        audio.volume -= soundVariation / 2f;
        
        audio.Play();
        
        audio.pitch = originalVolume;
        audio.volume = originalVolume;
    }

    public void ShootDefault() {
        PlaySound(_playerShootingDefault);
    }

    public void PlayerHit() {
        PlaySound(_playerHit);
    }

    public void EnemyHit() {
        PlaySound(_enemyHit);
    }

    public void DeathExplosion() {
        PlaySound(_deathExplosion);
    }

    public void Upgrade() {
        PlaySound(_upgrade);
    }

    public void PowerUp() {
        PlaySound(_powerUp);
    }

    public void ShootMachineGun() {
        PlaySound(_machineGun);
    }
}
