# Armed to the Teeth

Small arcadey 2D sidescroller shootemup I made with a tiny team for the Weekly Game Jam - Week 161. 

I wrote all the logic for the game, which included a player controller, basic upgrade system, a variety of enemy AI, an enemy spawner, and UI.

**[Check it out on itch.](https://enricogp.itch.io/armed-to-the-teeh)**