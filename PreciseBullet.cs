﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PreciseBullet : MonoBehaviour {
    
    [SerializeField] float _speed = 1f;
    Vector2 _target;

    void OnCollisionEnter2D(Collision2D collision) {
        if (collision.gameObject.tag == "Player") {
            collision.gameObject.GetComponent<PlayerController>().Damage();
            Destroy(this.gameObject);
        }
    }

    void Start() {
        _target = GameObject.FindGameObjectWithTag("Player").transform.position;
    }

    void Update() {
        
    }
}
