﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour {

    public string Name;
    public int CurrentAmmo;
    public int MaxAmmo = 0;
    public float Damage;
    public float FireRate;  // How long between shots (lower is faster)

    public void Fire() {

    }
    
}
