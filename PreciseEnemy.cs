﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PreciseEnemy : MonoBehaviour {
    
    [SerializeField] int _scoreValue;
    [Space]
    [SerializeField] float _maxHealth = 4f;
    [SerializeField] float _fireRate = 1f;
    [SerializeField] GameObject _preciseBulletPrefab;
    
    GameLogic _gameLogic;
    float _nextFire = 0f;
    float _currentHealth;

    void OnEnable() {
        _gameLogic = GameObject.Find("Game Manager").GetComponent<GameLogic>();
    }

    void Start() {
        _currentHealth = _maxHealth * _gameLogic.EnemyHealthMultiplier;
    }

    void OnCollisionEnter2D(Collision2D collision) {
        if (collision.gameObject.tag == "Player") {
            collision.gameObject.GetComponent<PlayerController>().Damage();
        }
    }

    void Update() {        
        if (Time.time > _nextFire) {
            var bullet = Instantiate(_preciseBulletPrefab, this.transform.position, Quaternion.identity);
            _nextFire = Time.time + 1f / _fireRate;
        }
    }

    public void Damage(float damage) {
        _currentHealth -= damage;
        if (_currentHealth <= 0f) {
            _gameLogic.Score += _scoreValue;
            Destroy(this.gameObject);
            --_gameLogic.EnemiesLeftBeforeUpgrade;
        }
    }
}
