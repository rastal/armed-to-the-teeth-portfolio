﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplodingRobot : MonoBehaviour {

    [HideInInspector] public bool Exploding = false;

    [SerializeField] int _scoreValue = 1;

    [Space]
    [SerializeField] float _moveSpeed = 1f;
    [SerializeField] float _seekSpeed = 1.5f;
    [SerializeField] float _maxHealth = 4f;
    [SerializeField] AnimationCurve _curve;
    
    GameLogic _gameLogic;
    AudioManager _audio;
    GameObject _player;
    ParticleSystem _explosion;
    SpriteRenderer _sprite;
    float _currentHealth;
    bool _deathCounted = false;

    void OnEnable() {
        _player = GameObject.FindWithTag("Player");
        _gameLogic = GameObject.Find("Game Manager").GetComponent<GameLogic>();
        _audio = GameObject.Find("Audio Manager").GetComponent<AudioManager>();
        _explosion = this.GetComponentInChildren<ParticleSystem>();
        _sprite = this.GetComponent<SpriteRenderer>();
    }

    void Start() {
        _currentHealth = _maxHealth * _gameLogic.EnemyHealthMultiplier;
    }

    void OnCollisionEnter2D(Collision2D collision) {
        if (collision.gameObject.tag == "Player") {
            collision.gameObject.GetComponent<PlayerController>().Damage();
            _explosion.Play();
            Exploding = true;
            _sprite.forceRenderingOff = true;
        }
    }

    void Update() {
        this.transform.Translate(new Vector3(-1f, 0f, 0f) * _moveSpeed * Time.deltaTime);

        if (this.transform.position.x < 10f) {    
            var distance = this.transform.position.y - _player.transform.position.y;
            var target = new Vector2(this.transform.position.x, _player.transform.position.y);
            this.transform.position = Vector2.Lerp(this.transform.position, target, Time.deltaTime * _seekSpeed);
        }

        if (this.transform.position.x < -10f) {
            Destroy(this.gameObject);
            --_gameLogic.EnemiesLeftBeforeUpgrade;
        }
        if (Exploding && !_explosion.IsAlive()) {
            Destroy(this.gameObject);
        }
        if (Exploding && !_deathCounted) {
            _deathCounted = true;
            --_gameLogic.EnemiesLeftBeforeUpgrade;
        }
    }

    public void Damage(float damage) {
        _currentHealth -= damage;
        if (_currentHealth <= 0f && !Exploding) {
            _audio.DeathExplosion();
            _gameLogic.Score += _scoreValue;
            _explosion.Play();
            Exploding = true;
            _sprite.forceRenderingOff = true;
            --_gameLogic.EnemiesLeftBeforeUpgrade;
        } else {
            _audio.EnemyHit();
        }
    }
}
