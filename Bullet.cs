﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Bullet : MonoBehaviour {
    
    public enum BulletOrigin {
        Player, 
        Enemy
    }

    [HideInInspector] public float Damage;
    [SerializeField] BulletOrigin _origin = BulletOrigin.Player;
    [SerializeField] float _bulletSpeed = 10f;
    int _direction = 1;

    void OnEnable() {
        if (_origin == BulletOrigin.Enemy) {
            _direction = -1;
        }
    }

    void OnCollisionEnter2D(Collision2D collision) {
        if (_origin == BulletOrigin.Enemy && collision.gameObject.tag == "Player") {
            
            collision.gameObject.GetComponent<PlayerController>().Damage();
            Destroy(this.gameObject);
        }

        if (_origin == BulletOrigin.Player && collision.gameObject.layer == 0 && 
                collision.gameObject.tag != "Player") {

            var commonEnemy = collision.gameObject.GetComponent<CommonEnemy>();
            if (commonEnemy != null) {
                commonEnemy.Damage(Damage);
                Destroy(this.gameObject);
            }

            var rotatingEnemy = collision.gameObject.GetComponent<RotatingEnemyShip>();
            if (rotatingEnemy != null) {
                rotatingEnemy.Damage(Damage);
                Destroy(this.gameObject);
            }

            var explodingRobot = collision.gameObject.GetComponent<ExplodingRobot>();
            if (explodingRobot != null && !explodingRobot.Exploding) {
                explodingRobot.Damage(Damage);
                Destroy(this.gameObject);
            }

            var preciseEnemy = collision.gameObject.GetComponent<PreciseEnemy>();
            if (preciseEnemy != null) {
                preciseEnemy.Damage(Damage);
                Destroy(this.gameObject);
            }

            if (collision.gameObject.GetComponent<ShieldBarrier>() != null) {
                Destroy(this.gameObject);
            }
        }
    }

    void Update() {
        this.transform.Translate(new Vector3(1f * _direction, 0f, 0f) * _bulletSpeed * Time.deltaTime);
        if (this.transform.position.x > 10f || this.transform.position.x < -10f) {
            Destroy(this.gameObject);
        }
    }
}
